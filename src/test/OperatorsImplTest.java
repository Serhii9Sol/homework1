package test;

import main.java.com.homework1.operators.Operators;
import main.java.com.homework1.operators.OperatorsImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OperatorsImplTest {

    Operators cut = new OperatorsImpl();

    @Test
    void evenNumberTest1() {
        int actual = cut.evenNumber(3, 5);
        int expected = 8;
        assertEquals(expected,actual);
    }

    @Test
    void evenNumberTest2() {
        int actual = cut.evenNumber(4, 5);
        int expected = 20;
        assertEquals(expected,actual);
    }


    @Test
    void quarterOfPointTest1() {
        String actual = cut.quarterOfPoint(0, 0);
        String expected = "ORIGIN";
        assertEquals(expected,actual);
    }

    @Test
    void quarterOfPointTest2() {
        String actual = cut.quarterOfPoint(0, 8);
        String expected = "OY_AXIS";
        assertEquals(expected,actual);
    }

    @Test
    void quarterOfPointTest3() {
        String actual = cut.quarterOfPoint(-5, 0);
        String expected = "OX_AXIS";
        assertEquals(expected,actual);
    }

    @Test
    void quarterOfPointTest4() {
        String actual = cut.quarterOfPoint(2, 2);
        String expected = "I_QUARTER";
        assertEquals(expected,actual);
    }

    @Test
    void quarterOfPointTest5() {
        String actual = cut.quarterOfPoint(2, -2);
        String expected = "IV_QUARTER";
        assertEquals(expected,actual);
    }

    @Test
    void quarterOfPointTest6() {
        String actual = cut.quarterOfPoint(-2, 2);
        String expected = "II_QUARTER";
        assertEquals(expected,actual);
    }

    @Test
    void quarterOfPointTest7() {
        String actual = cut.quarterOfPoint(-2, -2);
        String expected = "III_QUARTER";
        assertEquals(expected, actual);
    }

    @Test
    void sumOfPositiveNumbersTest1() {
        int actual = cut.sumOfPositiveNumbers(1, 2, 5);
        int expected = 8;
        assertEquals(expected, actual);
    }

    @Test
    void sumOfPositiveNumbersTest2() {
        assertEquals(0, cut.sumOfPositiveNumbers(-1, -2, -5));
    }

    @Test
    void sumOfPositiveNumbersTest3() {
        int actual = cut.sumOfPositiveNumbers(1, 2, -5);
        int expected = 3;
        assertEquals(expected, actual);
    }

    @Test
    void maxTest1() {
        int actual = cut.max(1, 2, -5);
        int expected = 1;
        assertEquals(expected, actual);
    }

    @Test
    void maxTest2() {
        int actual = cut.max(2, 3, 4);
        int expected = 27;
        assertEquals(expected, actual);
    }

    @Test
    void maxTest3() {
        int actual = cut.max(-2, -2, -2);
        int expected = -3;
        assertEquals(expected, actual);
    }

    @Test
    void ratingOfStudentTest1() {
        String actual = cut.ratingOfStudent(100);
        String expected = "A";
        assertEquals(expected, actual);
    }

    @Test
    void ratingOfStudentTest2() {
        String actual = cut.ratingOfStudent(-20);
        String expected = "Оцінка введена не корректно";
        assertEquals(expected, actual);
    }

    @Test
    void ratingOfStudentTest3() {
        String actual = cut.ratingOfStudent(150);
        String expected = "Оцінка введена не корректно";
        assertEquals(expected, actual);
    }
}