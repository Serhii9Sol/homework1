package test;

import main.java.com.homework1.function.Function;
import main.java.com.homework1.function.FunctionImpl;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

class FunctionImplTest {

    Function cut = new FunctionImpl();

//    ************Task1**************
    @Test
    void dayByNumberTest1() {
        Class expected = IllegalArgumentException.class;
        assertThrows(expected, () -> cut.dayByNumber(0));
    }

    @Test
    void dayByNumberTest2() {
        Class expected = IllegalArgumentException.class;
        assertThrows(expected, () -> cut.dayByNumber(10));
    }

    @Test
    void dayByNumberTest3() {
        String actual = cut.dayByNumber(1);
        String expected = "Monday";
        assertEquals(expected, actual);
    }

//    ***************Task2*******************
    @Test
    void distanceBetweenPointsTest1() {
        double actual = cut.distanceBetweenPoints(0, 0, 2, 2);
        double expected = 2.83;
        assertTrue(Math.abs(actual - expected) < 0.01);

    }

    @Test
    void distanceBetweenPointsTest2() {
        double actual = cut.distanceBetweenPoints(-1, -1, 1, -1);
        double expected = 2.00;
        assertTrue(Math.abs(actual - expected) < 0.01);

    }

    @Test
    void distanceBetweenPointsBigTest1(){
        BigInteger actual = cut.distanceBetweenPointsBig(new BigInteger("1"), new BigInteger("0"),
                new BigInteger("1"), new BigInteger("5"));
        BigInteger expected = new BigInteger("5");
        assertEquals(0, actual.compareTo(expected));
    }

    @Test
    void distanceBetweenPointsBigTest2(){
        BigInteger actual = cut.distanceBetweenPointsBig(new BigInteger("999000000000"), new BigInteger("999000000000"),
                new BigInteger("-999000000000"), new BigInteger("-999000000000"));
        BigInteger expected = new BigInteger("2825598697621");
        assertEquals(0, actual.compareTo(expected));
    }

//    *****************Task3****************
    @Test
    void numberToTextTest1() {
        String actual = cut.numberToText(200);
        String expected = "двести";
        assertEquals(expected, actual);
    }

    @Test
    void numberToTextTest2() {
        Class expected = IllegalArgumentException.class;
        assertThrows(expected, () -> cut.numberToText(1100));
    }

    @Test
    void numberToTextTest3() {
        String actual = cut.numberToText(0);
        String expected = "ноль";
        assertEquals(expected, actual);
    }

    @Test
    void numberToTextTest4() {
        String actual = cut.numberToText(740);
        String expected = "семьсот сорок";
        assertEquals(expected, actual);
    }

    @Test
    void numberToTextTest5() {
        String actual = cut.numberToText(52);
        String expected = "пятьдесят два";
        assertEquals(expected, actual);
    }

    @Test
    void numberToTextTest6() {
        String actual = cut.numberToText(13);
        String expected = "тринадцать";
        assertEquals(expected, actual);
    }

    @Test
    void numberToTextTest7() {
        String actual = cut.numberToText(477);
        String expected = "четыреста семьдесят семь";
        assertEquals(expected, actual);
    }

    @Test
    void numberToTextTest8() {
        Class expected = IllegalArgumentException.class;
        assertThrows(expected, () -> cut.numberToText(-5));
    }

    @Test
    void numberToTextTest9() {
        String actual = cut.numberToText(6);
        String expected = "шесть";
        assertEquals(expected, actual);
    }

//    *******************Task4*****************
    @Test
    void textToNumberTest1() {
        int actual = cut.textToNumber("   четыреста  семьдесят семь  ");
        int expected = 477;
        assertEquals(expected, actual);
    }

    @Test
    void textToNumberTest2() {
        int actual = cut.textToNumber("  семьдесят  ");
        int expected = 70;
        assertEquals(expected, actual);
    }

    @Test
    void textToNumberTest3() {
        int actual = cut.textToNumber("  пятьнадцать  ");
        int expected = 15;
        assertEquals(expected, actual);
    }

    @Test
    void textToNumberTest4() {
        Class expected = IllegalArgumentException.class;
        assertThrows(expected, () -> cut.textToNumber(" пятьнад цать"));
    }

    @Test
    void textToNumberTest5() {
        int actual = cut.textToNumber(" ноль");
        int expected = 0;
        assertEquals(expected, actual);
    }

    @Test
    void textToNumberTest6() {
        Class expected = IllegalArgumentException.class;
        assertThrows(expected, () -> cut.textToNumber(" ноль пятьнадцать"));
    }

    @Test
    void textToNumberTest7() {
        Class expected = IllegalArgumentException.class;
        assertThrows(expected, () -> cut.textToNumber("  триста десять триста"));
    }

    @Test
    void textToNumberTest8() {
        Class expected = IllegalArgumentException.class;
        assertThrows(expected, () -> cut.textToNumber(" десять сто"));
    }

    @Test
    void textToNumberTest9() {
        Class expected = IllegalArgumentException.class;
        assertThrows(expected, () -> cut.textToNumber(" десять сорок"));
    }

    @Test
    void textToNumberTest10() {
        Class expected = IllegalArgumentException.class;
        assertThrows(expected, () -> cut.textToNumber("пять тридцать восемьсот"));
    }

    @Test
    void textToNumberTest11() {
        Class expected = IllegalArgumentException.class;
        assertThrows(expected, () -> cut.textToNumber(" сорок девятьсот один"));
    }

    @Test
    void textToNumberTest12() {
        Class expected = IllegalArgumentException.class;
        assertThrows(expected, () -> cut.textToNumber(""));
    }

    @Test
    void textToNumberTest13() {
        Class expected = IllegalArgumentException.class;
        assertThrows(expected, () -> cut.textToNumber(null));
    }

    @Test
    void textToNumberTest14() {
        Class expected = IllegalArgumentException.class;
        assertThrows(expected, () -> cut.textToNumber("   "));
    }
}