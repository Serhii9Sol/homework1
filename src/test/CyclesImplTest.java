package test;

import main.java.com.homework1.cycles.Cycles;
import main.java.com.homework1.cycles.CyclesImpl;
import main.java.com.homework1.cycles.ResultOfMethod;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CyclesImplTest {

    Cycles cut = new CyclesImpl();

//    ***************Task1***************
    @Test
    void sumAndCountPositiveTest1() {
        ResultOfMethod actual = cut.sumAndCountPositive();
        ResultOfMethod expected = new ResultOfMethod(2450, 49);
        assertEquals(expected.count, actual.count);
        assertEquals(expected.sum, actual.sum);

    }

//    ***************Task2************
    @Test
    void isSimpleNumberTest1() {
        boolean actual = cut.isSimpleNumber(6);
//        boolean expected = false;
        assertFalse(actual);
    }

    @Test
    void isSimpleNumberTest2() {
        boolean actual = cut.isSimpleNumber(1);
//        boolean expected = false;
        assertFalse(actual);
    }

    @Test
    void isSimpleNumberTest3() {
        boolean actual = cut.isSimpleNumber(-5);
//        boolean expected = false;
        assertFalse(actual);
    }

    @Test
    void isSimpleNumberTest4() {
        boolean actual = cut.isSimpleNumber(5);
//        boolean expected = false;
        assertTrue(actual);
    }

    @Test
    void isSimpleNumberTest5() {
        boolean actual = cut.isSimpleNumber(199);
//        boolean expected = false;
        assertTrue(actual);
    }

//    ***************Task3**********
    @Test
    void rootTest1() {
        Class expected = IllegalArgumentException.class;
        assertThrows(expected, () -> cut.root(-9));
    }

    @Test
    void rootTest2() {
        int actual = cut.root(25);
        int expected = 5;
        assertEquals(expected, actual);
    }

    @Test
    void rootTest3() {
        int actual = cut.root(27);
        int expected = 5;
        assertEquals(expected, actual);
    }

    @Test
    void rootTest4() {
        int actual = cut.root(1);
        int expected = 1;
        assertEquals(expected, actual);
    }

    @Test
    void rootTest5() {
        int actual = cut.root(0);
        int expected = 0;
        assertEquals(expected, actual);
    }

    @Test
    void rootBinarTest1() {
        Class expected = IllegalArgumentException.class;
        assertThrows(expected, () -> cut.rootBinar(-9));
    }

    @Test
    void rootBinarTest2() {
        int actual = cut.rootBinar(25);
        int expected = 5;
        assertEquals(expected, actual);
    }

    @Test
    void rootBinarTest3() {
        int actual = cut.rootBinar(27);
        int expected = 5;
        assertEquals(expected, actual);
    }

    @Test
    void rootBinarTest4() {
        int actual = cut.rootBinar(1);
        int expected = 1;
        assertEquals(expected, actual);
    }

    @Test
    void rootBinarTest5() {
        int actual = cut.rootBinar(0);
        int expected = 0;
        assertEquals(expected, actual);
    }

    @Test
    void rootBinarTest6() {
        int actual = cut.rootBinar(2);
        int expected = 1;
        assertEquals(expected, actual);
    }

//    ***************Task4*********************
    @Test
    void factorialTest1() {
        Class expected = IllegalArgumentException.class;
        assertThrows(expected, () -> cut.factorial(-2));
    }

    @Test
    void factorialTest2() {
        long actual = cut.factorial(20);
        long expected = 2432902008176640000l;
        assertEquals(expected, actual);
    }

    @Test
    void factorialTest3() {
        long actual = cut.factorial(0);
        long expected = 1l;
        assertEquals(expected, actual);
    }

    @Test
    void factorialTest4() {
        long actual = cut.factorial(1);
        long expected = 1l;
        assertEquals(expected, actual);
    }


//    **************Task5***************
    @Test
    void sumOfDigitsInNumberTest1() {
        int actual = cut.sumOfDigitsInNumber(403);
        int expected = 7;
        assertEquals(expected, actual);
    }

    @Test
    void sumOfDigitsInNumberTest2() {
        int actual = cut.sumOfDigitsInNumber(-403);
        int expected = 7;
        assertEquals(expected, actual);
    }

    @Test
    void sumOfDigitsInNumberTest3() {
        int actual = cut.sumOfDigitsInNumber(-0);
        int expected = 0;
        assertEquals(expected, actual);
    }

//    *************Task6***************
    @Test
    void reverseNumberTest1() {
        long actual = cut.reverseNumber(12345);
        long expected = 54321l;
        assertEquals(expected, actual);
    }

    @Test
    void reverseNumberTest2() {
        long actual = cut.reverseNumber(1050);
        long expected = 501l;
        assertEquals(expected, actual);
    }

    @Test
    void reverseNumberTest3() {
        Class expected = IllegalArgumentException.class;
        assertThrows(expected, () -> cut.reverseNumber(-1050));
    }
}