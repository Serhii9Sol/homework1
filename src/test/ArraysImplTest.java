package test;

import main.java.com.homework1.arrays.Arrays;
import main.java.com.homework1.arrays.ArraysImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArraysImplTest {

    Arrays cut = new ArraysImpl();

    @Test
    void minOfArrayTest1() {
        int actual = cut.minOfArray(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int expected = -5;
        assertEquals(expected, actual);
    }

    @Test
    void maxOfArrayTest1() {
        int actual = cut.maxOfArray(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int expected = 22;
        assertEquals(expected, actual);
    }

    @Test
    void indexOfMinTest1() {
        int actual = cut.indexOfMin(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int expected = 7;
        assertEquals(expected, actual);
    }

    @Test
    void indexOfMaxTest1() {
        int actual = cut.indexOfMax(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int expected = 4;
        assertEquals(expected, actual);
    }

    @Test
    void sumOfElementWithOddIndexTest1() {
        int actual = cut.sumOfElementWithOddIndex(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int expected = 13;
        assertEquals(expected, actual);
    }

    @Test
    void reverseTest1() {
        int[] actual = cut.reverse(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int[] expected = new int[]{8,4,0,-5,17,10,22,2,3,2,1};
        assertArrayEquals(expected, actual);
    }

    @Test
    void countOddElementsTest1() {
        int actual = cut.countOddElements(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int expected = 4;
        assertEquals(expected, actual);
    }

    @Test
    void reverseHalfTest1() {
        int[] actual = cut.reverseHalf(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int[] expected = new int[]{17,-5,0,4,8,10,1,2,3,2,22};
        assertArrayEquals(expected, actual);
    }

    @Test
    void reverseHalfTest2() {
        int[] actual = cut.reverseHalf(new int[]{1,0,3,2,-2,10});
        int[] expected = new int[]{2,-2,10,1,0,3};
        assertArrayEquals(expected, actual);
    }

    @Test
    void bubbleSortingTest1() {
        int[] actual = cut.bubbleSorting(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int[] expected = new int[]{-5,0,1,2,2,3,4,8,10,17,22};
        assertArrayEquals(expected, actual);
    }

    @Test
    void selectSortingTest1() {
        int[] actual = cut.selectSorting(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int[] expected = new int[]{-5,0,1,2,2,3,4,8,10,17,22};
        assertArrayEquals(expected, actual);
    }

    @Test
    void insertSortingTest1() {
        int[] actual = cut.insertSorting(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int[] expected = new int[]{-5,0,1,2,2,3,4,8,10,17,22};
        assertArrayEquals(expected, actual);
    }

    @Test
    void quickSortingTest1() {
        int[] actual = cut.quickSorting(new int[]{1,2,3,2,22,10,17,-5,0,4,8}, 0, 10);
        int[] expected = new int[]{-5,0,1,2,2,3,4,8,10,17,22};
        assertArrayEquals(expected, actual);
    }

    @Test
    void mergeSortingTest1() {
        int[] actual = cut.mergeSorting(new int[]{1,2,3,2,22,10,17,-5,0,4,8}, 0, 10);
        int[] expected = new int[]{-5,0,1,2,2,3,4,8,10,17,22};
        assertArrayEquals(expected, actual);
    }

    @Test
    void shellSortingTest1() {
        int[] actual = cut.shellSorting(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int[] expected = new int[]{-5,0,1,2,2,3,4,8,10,17,22};
        assertArrayEquals(expected, actual);
    }

    @Test
    void heapSortingTest1() {
        int[] actual = cut.heapSorting(new int[]{1,2,3,2,22,10,17,-5,0,4,8});
        int[] expected = new int[]{-5,0,1,2,2,3,4,8,10,17,22};
        assertArrayEquals(expected, actual);
    }
}