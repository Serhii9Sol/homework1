package main.java.com.homework1.cycles;

public class CyclesImpl implements Cycles {

    public ResultOfMethod sumAndCountPositive(){
        int sum = 0;
        int count = 0;
        for(int i = 1; i <= 99; i++){
            if( i % 2 == 0){
                sum += i;
                count ++;
            }
        }
        return new ResultOfMethod(sum, count);
    }

    public boolean isSimpleNumber(int number){
        if(number <= 1){
            return false;
        }
        for( int i = 2; i <= number - 1; i++){
            if( number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public int root(int number){
        if(number < 0){
            throw new IllegalArgumentException("отрицательное число");
        }

        if(number == 0){
            return 0;
        }

        int root = 1;
        for(int i = 1; i <= number; i++){
            if( ( i * i ) == number) {
                root = i;
                break;
            }else if( ( i * i ) > number){
                root = i - 1;
                break;
            }
        }
        return root;
    }

    public int rootBinar(int number){
        if(number < 0){
            throw new IllegalArgumentException("отрицательное число");
        }

        if(number == 1){
            return 1;
        }

        int low = 0;
        int high = number;
        int root = 0;
        while(true) {
            root = (low + high) / 2;
            if(root * root <= number && (root +1) * (root +1) > number){
                break;
            }
            if(root * root >= number){
                high = root;
            } else {
                low = root;
            }
        }
        return root;
    }

    public long factorial(int number){
        if(number < 0){
            throw new IllegalArgumentException("отрицательное число");
        }
        long factorial = 1L;
        for( int i = 1; i <= number; i++ ){
            factorial = factorial * i;
        }
        return factorial;
    }

    public int sumOfDigitsInNumber(long number){
        number = Math.abs(number);
        String [] stringNumber = String.valueOf(number).split("");
        int sum = 0;
        for(int i = 0; i < stringNumber.length; i++ ){
            sum = sum + Integer.parseInt( stringNumber[i] );
        }
        return sum;
    }

    public long reverseNumber(long number){
//        return Long.parseLong(new StringBuilder(String.valueOf(number)).reverse().toString());
        if(number < 0){
            throw new IllegalArgumentException();
        }
        long reversNumber = 0;
        while(number != 0){
            reversNumber = reversNumber * 10 + number % 10;
            number = number / 10;
        }
        return reversNumber;
    }
}
