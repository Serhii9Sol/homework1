package main.java.com.homework1.cycles;

public interface Cycles {

    ResultOfMethod sumAndCountPositive();

    boolean isSimpleNumber(int number);

    int root(int number);

    int rootBinar(int number);

    long factorial(int number);

    int sumOfDigitsInNumber(long number);

    long reverseNumber(long number);
}
