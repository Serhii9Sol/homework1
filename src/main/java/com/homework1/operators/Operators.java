package main.java.com.homework1.operators;

public interface Operators {

    int evenNumber(int a, int b);

    String quarterOfPoint(int x, int y);

    int sumOfPositiveNumbers(int x1, int x2, int x3);

    int max(int a, int b, int c);

    String ratingOfStudent(int a);
}
