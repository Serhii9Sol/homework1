package main.java.com.homework1.operators;

public class OperatorsImpl implements Operators{

    public int evenNumber(int a, int b){
        if( a%2 == 0 ){
            return a * b;
        } else {
            return a + b;
        }
    }

    public String quarterOfPoint(int x, int y) {
        String position = "";
        if (x == 0) {
            if (y == 0) {
                position = "ORIGIN";
            } else {
                position = "OY_AXIS";
            }
        } else if (y == 0) {
            position = "OX_AXIS";
        } else {
                if (x > 0 && y > 0) {
                    position = "I_QUARTER";
                } else if (x > 0 && y < 0) {
                    position = "IV_QUARTER";
                } else if (x < 0 && y > 0) {
                    position = "II_QUARTER";
                } else if (x < 0 && y < 0) {
                    position = "III_QUARTER";
                }
        }
        return position;
    }

    public int sumOfPositiveNumbers(int x1, int x2, int x3){
        int sum = 0;
        if ( x1 > 0 ) {
            sum += x1;
        }
        if ( x2 > 0 ) {
            sum += x2;
        }
        if ( x3 > 0 ) {
            sum += x3;
        }
        return sum;
    }

    public int max(int a, int b, int c){
        if ( a * b * c > ( a + b + c ) ) {
            return a * b * c + 3;
        } else {
            return a + b + c + 3;
        }
    }

    public String ratingOfStudent(int a){
        if ( a >= 0 ){
            if( a <= 19 ){
                return "F";
            }else if ( a <= 39 ){
                return "E";
            }else if ( a <= 59 ){
                return "D";
            }else if ( a <= 74 ){
                return "C";
            }else if ( a <= 89 ){
                return "B";
            }else if ( a <= 100 ){
                return "A";
            }
        }

        return "Оцінка введена не корректно";
    }
}
