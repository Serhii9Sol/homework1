package main.java.com.homework1.function;

import java.math.BigInteger;

public class FunctionImpl implements Function{

    public String dayByNumber(int n){
        if (n < 1 || n > 7){
            throw new IllegalArgumentException();
        }
        String day = "";
        switch(n){
            case 1:
                day = "Monday";
                break;
            case 2:
                day = "Tuesday";
                break;
            case 3:
                day = "Wednesday";
                break;
            case 4:
                day = "Thursday";
                break;
            case 5:
                day = "Friday";
                break;
            case 6:
                day = "Saturday";
                break;
            case 7:
                day = "Sunday";
                break;
        }
        return day;
    }

    public double distanceBetweenPoints(int x1, int y1, int x2, int y2){
        return Math.sqrt(Math.pow((x2-x1),2) + Math.pow((y2-y1),2));
    }

    public BigInteger distanceBetweenPointsBig(BigInteger x1, BigInteger y1, BigInteger x2, BigInteger y2){
        BigInteger xSubtract = x1.subtract(x2);
        BigInteger ySubtract = y1.subtract(y2);
        BigInteger result = xSubtract.multiply(xSubtract).add(ySubtract.multiply(ySubtract)).sqrt();
        return result;
    }

    public String numberToText(int number){
        if( number < 0 || number > 999){
            throw new IllegalArgumentException();
        }
        if(number == 0){
            return "ноль";
        }
        int hundred = number / 100;
        number = number % 100;
        int ten = number / 10;
        int units = number % 10;

        String strHundred = "";
        String strTen = "";
        String strUnits = "";

        switch (hundred){
            case 1:
                strHundred = "сто ";
                break;
            case 2:
                strHundred = "двести ";
                break;
            case 3:
                strHundred = "триста ";
                break;
            case 4:
                strHundred = "четыреста ";
                break;
            case 5:
                strHundred = "пятьсот ";
                break;
            case 6:
                strHundred = "шестьсот ";
                break;
            case 7:
                strHundred = "семьсот ";
                break;
            case 8:
                strHundred = "восемьсот ";
                break;
            case 9:
                strHundred = "девятьсот ";
                break;
        }

        switch (ten){
            case 2:
                strTen = "двадцать ";
                break;
            case 3:
                strTen = "тридцать ";
                break;
            case 4:
                strTen = "сорок ";
                break;
            case 5:
                strTen = "пятьдесят ";
                break;
            case 6:
                strTen = "шестьдесят ";
                break;
            case 7:
                strTen = "семьдесят ";
                break;
            case 8:
                strTen = "восемьдесят ";
                break;
            case 9:
                strTen = "девяносто ";
                break;
        }

        if (ten == 1){
            switch (units){
                case 0:
                    strUnits = "десять";
                    break;
                case 1:
                    strUnits = "одинадцать";
                    break;
                case 2:
                    strUnits = "двенадцать";
                    break;
                case 3:
                    strUnits = "тринадцать";
                    break;
                case 4:
                    strUnits = "четырнадцать";
                    break;
                case 5:
                    strUnits = "пятьнадцать";
                    break;
                case 6:
                    strUnits = "шестьнадцать";
                    break;
                case 7:
                    strUnits = "семьнадцать";
                    break;
                case 8:
                    strUnits = "восемьнадцать";
                    break;
                case 9:
                    strUnits = "девятьнадцать";
                    break;
            }
        } else {
            switch (units){
                case 1:
                    strUnits = "один";
                    break;
                case 2:
                    strUnits = "два";
                    break;
                case 3:
                    strUnits = "три";
                    break;
                case 4:
                    strUnits = "четыре";
                    break;
                case 5:
                    strUnits = "пять";
                    break;
                case 6:
                    strUnits = "шесть";
                    break;
                case 7:
                    strUnits = "семь";
                    break;
                case 8:
                    strUnits = "восемь";
                    break;
                case 9:
                    strUnits = "девять";
                    break;
            }
        }

        String result = strHundred + strTen + strUnits;

        return result.trim();
    }

    public int textToNumber(String strNumber){

        if(strNumber == null){
            throw new IllegalArgumentException("нельзя передавать NULL");
        }

        if(strNumber.isEmpty()){
            throw new IllegalArgumentException("нельзя передавать пустую строку");
        }

        if(strNumber.trim().equals("ноль")){
            return 0;
        }

        String[] partOfNumber = strNumber.trim().split("\\s+");
        int length = partOfNumber.length;
        if(length == 0 || length > 3){
            throw new IllegalArgumentException("введено много слов");
        }

        int hundred = 0;
        int ten = 0;
        int unit = 0;

        for(int i = 0; i < length; i++){
                switch(partOfNumber[i]){
                    case "сто":
                        if(hundred != 0 || ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        hundred = 100;
                        break;
                    case "двести":
                        if(hundred != 0 || ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        hundred = 200;
                        break;
                    case "триста":
                        if(hundred != 0 || ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        hundred = 300;
                        break;
                    case "четыреста":
                        if(hundred != 0 || ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        hundred = 400;
                        break;
                    case "пятьсот":
                        if(hundred != 0 || ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        hundred = 500;
                        break;
                    case "шестьсот":
                        if(hundred != 0 || ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        hundred = 600;
                        break;
                    case "семьсот":
                        if(hundred != 0 || ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        hundred = 700;
                        break;
                    case "восемьсот":
                        if(hundred != 0 || ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        hundred = 800;
                        break;
                    case "девятьсот":
                        if(hundred != 0 || ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        hundred = 900;
                        break;
                    case "десять":
                        if(ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        ten = 10;
                        break;
                    case "одинадцать":
                        if(ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        ten = 10;
                        unit = 1;
                        break;
                    case "двенадцать":
                        if(ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        ten = 10;
                        unit = 2;
                        break;
                    case "тринадцать":
                        if(ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        ten = 10;
                        unit = 3;
                        break;
                    case "четырнадцать":
                        if(ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        ten = 10;
                        unit = 4;
                        break;
                    case "пятьнадцать":
                        if(ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        ten = 10;
                        unit = 5;
                        break;
                    case "шестьнадцать":
                        if(ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        ten = 10;
                        unit = 6;
                        break;
                    case "семьнадцать":
                        if(ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        ten = 10;
                        unit = 7;
                        break;
                    case "восемьнадцать":
                        if(ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        ten = 10;
                        unit = 8;
                        break;
                    case "девятьнадцать":
                        if(ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        ten = 10;
                        unit = 9;
                        break;
                    case "двадцать":
                        if(ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        ten = 20;
                        break;
                    case "тридцать":
                        if(ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        ten = 30;
                        break;
                    case "сорок":
                        if(ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        ten = 40;
                        break;
                    case "пятьдесят":
                        if(ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        ten = 50;
                        break;
                    case "шестьдесят":
                        if(ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        ten = 60;
                        break;
                    case "семьдесят":
                        if(ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        ten = 70;
                        break;
                    case "восемьдесят":
                        if(ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        ten = 80;
                        break;
                    case "девяносто":
                        if(ten != 0 || unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        ten = 90;
                        break;
                    case "один":
                        if(unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        unit = 1;
                        break;
                    case "два":
                        if(unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        unit = 2;
                        break;
                    case "три":
                        if(unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        unit = 3;
                        break;
                    case "четыре":
                        if(unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        unit = 4;
                        break;
                    case "пять":
                        if(unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        unit = 5;
                        break;
                    case "шесть":
                        if(unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        unit = 6;
                        break;
                    case "семь":
                        if(unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        unit = 7;
                        break;
                    case "восемь":
                        if(unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        unit = 8;
                        break;
                    case "девять":
                        if(unit != 0){
                            throw new IllegalArgumentException("неправильный порядок или повтор слов");
                        }
                        unit = 9;
                        break;
                    default :
                        throw new IllegalArgumentException("\"" + partOfNumber[i] + "\"" + " написано неверно");
                }
        }
        return hundred + ten + unit;
    }
}
