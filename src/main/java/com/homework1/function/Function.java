package main.java.com.homework1.function;

import java.math.BigInteger;

public interface Function {

    String dayByNumber(int n);

    double distanceBetweenPoints(int x1, int y1, int x2, int y2);

    BigInteger distanceBetweenPointsBig(BigInteger x1, BigInteger y1, BigInteger x2, BigInteger y2);

    String numberToText(int number);

    int textToNumber(String strNumber);
}
