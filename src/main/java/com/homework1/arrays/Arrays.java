package main.java.com.homework1.arrays;

public interface Arrays {

    int minOfArray(int[] arr);

    int maxOfArray(int[] arr);

    int indexOfMin(int[] arr);

    int indexOfMax(int[] arr);

    int sumOfElementWithOddIndex(int [] arr);

    int[] reverse(int [] arr);

    int countOddElements(int [] arr);

    int[] reverseHalf(int [] arr);

    int[] bubbleSorting(int [] arr);

    int[] selectSorting(int [] arr);

    int[] insertSorting(int [] arr);

    int[] quickSorting(int[] array, int low, int high);

    int[] mergeSorting(int[] array, int lowInd, int highInd);

    int[] shellSorting(int[] array);

    int[] heapSorting(int[] array);
}
