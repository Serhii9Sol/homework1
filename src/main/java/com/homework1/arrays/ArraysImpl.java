package main.java.com.homework1.arrays;

public class ArraysImpl implements Arrays{

    public int minOfArray(int[] arr){
        int min = arr[0];
        for(int i = 1; i < arr.length; i++){
            if(arr[i] < min){
                min = arr[i];
            }
        }
        return min;
    }

    public int maxOfArray(int[] arr){
        int max = arr[0];
        for(int i = 1; i < arr.length; i++){
            if(arr[i] > max){
                max = arr[i];
            }
        }
        return max;
    }

    public int indexOfMin(int[] arr){
        int min = arr[0];
        int minInd = 0;
        for(int i = 1; i < arr.length; i++){
            if(arr[i] < min){
                min = arr[i];
                minInd = i;
            }
        }
        return minInd;
    }

    public int indexOfMax(int[] arr){
        int max = arr[0];
        int maxInd = 0;
        for(int i = 1; i < arr.length; i++){
            if(arr[i] > max){
                max = arr[i];
                maxInd = i;
            }
        }
        return maxInd;
    }

    public int sumOfElementWithOddIndex(int [] arr){
        int sum = 0;
        for(int i = 0; i < arr.length; i++){
            if( i % 2 != 0) {
                sum += arr[i];
            }
        }
        return sum;
    }

    public int[] reverse(int [] arr){
        int reverseArr[] = new int[arr.length];
        for(int i = 0; i < arr.length; i++){
            reverseArr[i] = arr[arr.length-1-i];
        }
        return reverseArr;
    }

    public int countOddElements(int [] arr){
        int sum = 0;
        for(int i=0;i<arr.length;i++){
            if(arr[i]%2!=0) {
                sum++;
            }
        }
        return sum;
    }

    public int[] reverseHalf(int [] arr){
        int half = arr.length / 2;
        int centr = half + arr.length % 2;
        int buffer = 0;
        for (int i = 0; i < half; i++) {
            buffer = arr[i];
            arr[i] = arr[centr + i];
            arr[centr + i] = buffer;
        }
        return arr;
    }

    public int[] bubbleSorting(int [] arr){
        int[] sorted = java.util.Arrays.copyOf(arr,arr.length);
        boolean isSorted = false;
        int buffer;
        while(!isSorted) {
            isSorted = true;
            for (int i = 0; i < sorted.length-1; i++) {
                if(sorted[i] > sorted[i+1]){
                    isSorted = false;
                    buffer = sorted[i];
                    sorted[i] = sorted[i+1];
                    sorted[i+1] = buffer;
                }
            }
        }
        return sorted;
    }

    public int[] selectSorting(int [] arr){
        int[] sorted = java.util.Arrays.copyOf(arr,arr.length);
        int indexMin = 0;
        int min = 0;
        for (int i = 0; i < sorted.length; i++) {
            indexMin = i;
            min = sorted[i];
            for (int j = i + 1; j < sorted.length; j++) {
                if (sorted[j] < min) {
                    indexMin = j;
                    min = sorted[j];
                }
            }
            sorted[indexMin] = sorted[i];
            sorted[i] = min;
        }
        return sorted;
    }

    public int[] insertSorting(int [] arr){
        int[] sorted = java.util.Arrays.copyOf(arr,arr.length);
        int current = 0;
        int j = 0;
        for (int i = 1; i < sorted.length; i++) {
            current = sorted[i];
            j = i - 1;
            while(j >= 0 && current < sorted[j]) {
                sorted[j+1] = sorted[j];
                j--;
            }
            sorted[j+1] = current;
        }
        return sorted;
    }

    public int[] quickSorting(int[] array, int lowInd, int highInd) {
        //завершить выполнение, если длина массива равна 0
        if (array.length == 0) {
            return array;
        }

        //завершить выполнение если масив уже слижком маленький
        if (lowInd >= highInd){
            return array;
        }

        // выбрать опорный элемент
        int mid = array[lowInd + (highInd - lowInd) / 2];

        // разделить на подмассивы, который больше и меньше опорного элемента
        int i = lowInd;
        int j = highInd;
        while (i <= j) {
            //ищем индекс элемента большего чем опорный в левом масиве
            while (array[i] < mid) {
                i++;
            }

            //ищем индекс элемента меньшего чем опорный в правом масиве
            while (array[j] > mid) {
                j--;
            }

            //меняем местами
            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }

        // вызов рекурсии для сортировки левой и правой части
        if (lowInd < j)
            quickSorting(array, lowInd, j);

        if (highInd > i)
            quickSorting(array, i, highInd);

        return array;
    }

    public int[] mergeSorting(int[] array, int lowInd, int highInd) {

        int midInd = lowInd + (highInd - lowInd) / 2;
        int[] buffer = new int[highInd - lowInd + 1];
        //завершить выполнение если масив уже слижком маленький
        if (lowInd >= highInd) {
            buffer[0] = array[lowInd];
            return buffer;
        }

        //вызов рекурсии для сортировки левой и правой части
        int[] firstArray = mergeSorting(array, lowInd, midInd);
        int[] secondArray = mergeSorting(array, midInd + 1, highInd);
        //слияние масивов
        int indexFirstArray = 0;
        int indexSecondArray = 0;

        for (int bufferInd = 0; bufferInd < buffer.length; bufferInd++) {
            if (indexFirstArray > firstArray.length - 1) {
                buffer[bufferInd] = secondArray[indexSecondArray];
                indexSecondArray++;
            } else if (indexSecondArray > secondArray.length - 1) {
                buffer[bufferInd] = firstArray[indexFirstArray];
                indexFirstArray++;
            } else if (firstArray[indexFirstArray] < secondArray[indexSecondArray]) {
                buffer[bufferInd] = firstArray[indexFirstArray];
                indexFirstArray++;
            } else {
                buffer[bufferInd] = secondArray[indexSecondArray];
                indexSecondArray++;
            }
        }
        return buffer;
    }

    public int[] shellSorting(int[] array){
        int arrayLength =  array.length;
        for (int step = arrayLength/ 2; step > 0; step /= 2) {
            for (int i = step; i < arrayLength; i++) {
                for (int j = i - step; j >= 0 && array[j] > array[j + step] ; j -= step) {
                    int buffer = array[j];
                    array[j] = array[j + step];
                    array[j + step] = buffer;
                }
            }
        }
        return array;
    }
    
    public int[] heapSorting(int[] array) {
        int n = array.length;
        // Построение кучи (перегруппируем массив)
        for (int i = n / 2 - 1; i >= 0; i--)
            heapify(array, n, i);
        // Один за другим извлекаем элементы из кучи   
        for (int i=n-1; i>=0; i--)
        {
            // Перемещаем текущий корень в конец
            int buffer = array[0];
            array[0] = array[i];
            array[i] = buffer;
            // Вызываем процедуру heapify на уменьшенной куче
            heapify(array, i, 0);
        }
        return array;
    }

    private void heapify(int array[], int n, int i)
    {
        int largest = i; // Инициализируем наибольший элемент как корень
        int l = 2 * i + 1; // левый = 2*i + 1
        int r = 2 * i + 2; // правый = 2*i + 2

        // Если левый дочерний элемент больше корня
        if (l < n && array[l] > array[largest])
            largest = l;

        // Если правый дочерний элемент больше, чем самый большой элемент на данный момент
        if (r < n && array[r] > array[largest])
            largest = r;
        // Если самый большой элемент не корень
        if (largest != i)
        {
            int buffer = array[i];
            array[i] = array[largest];
            array[largest] = buffer;

            // Рекурсивно преобразуем в двоичную кучу затронутое поддерево
            heapify(array, n, largest);
        }
    }
}
